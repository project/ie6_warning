$(document).ready(function() {

  // create a cookies manager
  var ie6_warning_cookies = new CookieHandler();

  // function to not display the warning
  function ie6_warning_hide() {
    $('.ie6_warning').hide();
  }
  
  // function to slowly hide the warning, and set the cookie
  function ie6_warning_close() {
    ie6_warning_cookies.setCookie('ie6_warning', 'ok', 365*60*60);
    $('.ie6_warning').slideUp('slow')();
  }
  
  // function to open the warning, and create the 'close' event
  function ie6_warning_open() {
    if (ie6_warning_cookies.getCookie('ie6_warning') == 'ok') {
      ie6_warning_hide();
    } else {
      $('.ie6_warning').slideDown('slow');
      $('.ie6_warning_close').click(function() {
        ie6_warning_close();
      })  
    }
  }
  
  // we display the warning only for IE6 users
  if ($.browser.msie() && $.browser.version.number() == 6) {
    ie6_warning_open();
  } else {
    ie6_warning_hide();
  }
});




/**
*
*  Javascript cookies
*  http://www.webtoolkit.info/
*  MIT license
*
**/
 
function CookieHandler() {
 
  this.setCookie = function (name, value, seconds) {
 
    if (typeof(seconds) != 'undefined') {
      var date = new Date();
      date.setTime(date.getTime() + (seconds*1000));
      var expires = "; expires=" + date.toGMTString();
    }
    else {
      var expires = "";
    }
 
    document.cookie = name+"="+value+expires+"; path=/";
  }
 
  this.getCookie = function (name) {
 
    name = name + "=";
    var carray = document.cookie.split(';');
 
    for(var i=0;i < carray.length;i++) {
      var c = carray[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
 
    return null;
  }
 
  this.deleteCookie = function (name) {
    this.setCookie(name, "", -1);
  }
 
}




