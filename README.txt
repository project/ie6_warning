// $Id: 

Description
-----------

IE6 warning displays a block exhorting IE6 users to upgrade their
browsers. The block is displayed on every page, but the user can close it
with a link on the top right corner of the block. Upon closing the block,
a cookie is set so that the block doesn't show anymore.

The message displayed in the block can be set by the administrator.

To play well with sites caching the content aggressively, everything is
happening client-side: the block is always sent, hidden, and IE6 users
have the block shown via Javascript.


Install
-------
Installing the IE6 warning module is simple:

1) Copy the ie6_warning folder to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)


Configuration
-------------

1) Configure the site-wide setting for IE6 warning, Administer -> Settings ->
   IE6 warning message. Eventually change the message.

2) Activate the options IE6 warning block, Administer -> Site building -> 
   Blocks. Set the block to be displayed in the header for instance

3) Find a version of IE6, and try it!
   
   
Customisation
-------------

1) You can of course change the message in the module settings

2) You can edit the CSS in ie6_warning/css/ie6_warning.css


Misc
----

This module uses the jQuery plugin jQBrowser (c) Dave Cardwell
http://davecardwell.co.uk/javascript/jquery/plugins/jquery-browserdetect/

